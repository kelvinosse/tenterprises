import 'buefy/lib/buefy.css';
import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
import Vue from 'vue';
import Buefy from 'buefy';
import VueScrollReveal from 'vue-scroll-reveal';
import vueSmoothScroll from 'vue-smooth-scroll';
import App from './App';
import router from './router';

Vue.use(Buefy);
Vue.use(VueScrollReveal);
Vue.use(vueSmoothScroll);
Vue.component('icon', Icon);
Vue.config.productionTip = false;

router.beforeResolve((to, from, next) => {
  if (to.name) {
    NProgress.start(); // eslint-disable-line
  }
  next();
});

router.afterEach(() => {
  NProgress.done(); // eslint-disable-line
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
});
